from bitcoinrpc.authproxy import AuthServiceProxy
from api.models import coins
from django.http import JsonResponse


class Wallet:
    rpc_connection = 0

    def __init__(self, coin_id):
        coin = coins.objects.get(id=coin_id)
        self.rpc_connection = AuthServiceProxy("http://" + coin.wallet_connection)

    def getinfo(self):
        return self.rpc_connection.getinfo()

    def getaddressesbyaccount(self, account):
        return self.rpc_connection.getaddressesbyaccount(account)

    def getnewaddress(self, account):
        return self.rpc_connection.getnewaddress(account)

    def gettransaction(self, txid):
        return self.rpc_connection.gettransaction(txid)

    def sendmany(self, to_many, minconf, comment=""):
        response = []
        try:
            response={'success': self.rpc_connection.sendmany('', to_many, minconf, comment)}
        except Exception as e:
            response={'error': str(e.message)}
        return response
    def getaddressesbyaccount(self, account):
        return self.rpc_connection.getaddressesbyaccount(account)

    def listtransactions(self, account):
        return self.rpc_connection.listtransactions(account)
