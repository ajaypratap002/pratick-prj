class MetaConst(type):
    # Author : Astuto Lee (2018-01-05)
    # Created for manage constant/messages
    def __getattr__(cls, key):
        return cls[key]

    def __setattr__(cls, key, value):
        raise TypeError


class Const(object):
    # Author : Astuto Lee (2018-01-05)
    # Created for manage constant/messages
    __metaclass__ = MetaConst

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, name, value):
        raise TypeError


class MESSAGES(Const):
    # Author : Astuto Lee (2018-01-05)
    # Created for manage constant/messages
    # COMMON IN MANY PAGE
    IS_NOT_VALID = " is not valid."
    CREATED_SUCCESS = " Created Successfully."
    SEND_SUCCESS = " Successfully sent "
    HOST = "http://localhost:5000/#!"
    SUCCESS_MSG = "Success"
    # ===================== user.py =======================================
    # User Registration Page
    BCC_ACTIVATION_SUBJECT = "BCCexchange activation"
    BCC_EMAIL = 'activate@bccexchnage.co'
    REGISTERED_NO_EMAIL_SEND = "You have registered successfully. But coudn't send email."
    REGISTERED_EMAIL_SEND = "You have registered successfully. Please check your email for activation."
    # User available or not
    USERNAME_UNAVAILABLE = "Username is not available."
    USERNAME_AVAILABLE = "Username is available."
    # account activation
    ACCOUNT_VERIFIED = "Your account has been activated successfully."
    ACCOUNT_ALREADY_VERIFIED = "The account with this email id is already active."
    ACCOUNT_NOT_VERIFICATION_FAIL = "Account verification failed. Please try again."
    # forgot
    EMAIL_NOT_MATCH = "Email doesn't match our records."
    RESET_PASSWORD_SUBJECT = "Reset Password"
    EMAIL_SEND_ERROR = "Error While Sending email."
    RESET_PASSWORD_EMAIL = 'BCCExchange <forgot@bccexchange.co>'
    RESET_PASSWORD_EMAIL_SEND = "Password reset link has been successfully send on your email."
    # forgotReset

    # security - Change Password & forgotReset
    SECURITY_SUBJECT = "Change Password"
    SECURITY_EMAIL = 'BCCExchange <forget@bccexchange.co>'
    PASSWORD_CHANGED = "Your password has been successfully changed."
    PASSWORD_NOT_MATCH = "Your current password didn't match."
    ADDRESS_NOT_MATCH = "Invalid"
    # profile update
    PROFILE_UPDATED = "Your profile has been successfully updated."
    PROFILE_UPDATED_ERROR = "There is some issue while updating your profile."
    # UserBalanceViewSet & UserBalancesViewSet
    DISPLAY_BALANCE_ERROR = "There is an error display balance."

    # ===================== coin.py =======================================
    # CoinViewSet
    COIN_CREAT_ERROR = "There is an error while creating coin."
    # CoinList
    COIN_LIST_ERROR = "There is an error while getting coin list."

    # ===================== trading.py =======================================
    # BuyViewSet & SellViewSet
    REQUEST_SEND = "Your request was success fully send."

    # ===================== useraddress.py =======================================
    # UserAddressViewSet & generateAddress
    OPERATION_NOT_ALLOW = "You can't perform this operation."
    ADDRESS_CHECK_ERROR = "There is an error checking address."
    NEW_ADDRESS_ERROR = "There is an error display new address."

    # ===================== wallettransaction.py =======================================
    # TempTransactionsViewSet
    SERVICE_UNAVAILABLE = "This service is temporary unavailable."
    INSUFFICIENT_BALANCE = "No Sufficient Balance for "  # Half message
    # BtcTransactionDetailViewSet
    TRANSACTION_SEND_SUBJECT = "Amount sent Successfully"
    TRANSACTION_SEND_EMAIL = 'BCCExchnage <info@bccexchange.co>'
    TRANSACTION_SEND_EMAIL_NOT_SEND = "Successfully amount sent but coudn't send email."
    # BtcTransactionDetailViewSet
    TRANSACTION_DETAIL_DISPLAY_ERROR = "There is an error while displaying transaction details."
    # userWithdrawals
    SOMETHING_WRONG = "Something wrong"
    # cron
    UPDATE_INVALID_ADDRESS_STATUS = "Updated invalid Address Status."
    INSERT_ERROR_TBL = "Inserted in error table."
    TOTAL_ROW_NOT_MATCH = "Total row doesn't matched."
    # receive_wallet_notify
    INSERT_TRANSACTION_TBL = "Inserted in btc transaction table."
    OLD_TRANSACTION = "Too old transaction."
    USER_BALANCE_UPDATE = "user balance updated with transaction detail is_balance_update to 1"
    ERROR_IN_INSERT = "Error while inserting in table"
    # insert_transaction_detail
    INSERT_SUCCESS = "Data inserted in table"
    INSERT_ALREADY_EXISTS = "Data already exists while inserting transaction detail"
    # ===================== wallettransaction.py =======================================
    # custom_jwt_response
    ACCOUNT_NOT_VERIFIED = 'Your account is not verified yet.'
    ACCOUNT_NOT_ACTIVE = 'Your account is not active yet.'
