"""bccexchange URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns
from api.views.user import UserViewSet, UserBalanceViewSet, UserBalancesViewSet
from api.views.trading import BuyViewSet, SellViewSet, BidsAsksViewset
from api.views.useraddress import UserAddressViewSet
from api.views.test import TestViewSet
from api.views.coin import CoinViewSet, CoinList
from api.views.wallettransaction import TempTransactionsViewSet, BtcTransactionDetailViewSet, GetUserTransactions, cron, receive_wallet_notify
from rest_framework import renderers
from rest_framework import serializers
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token
from rest_framework_jwt.views import refresh_jwt_token

# from bccexchange.myconstants import mycont
# import bccexchange.myconstants

user = UserViewSet.as_view({
    'post': 'register'
}, renderer_classes=[renderers.StaticHTMLRenderer])

useraddress = UserAddressViewSet.as_view({
    'post': 'create'
}, renderer_classes=[renderers.StaticHTMLRenderer])

coin = CoinViewSet.as_view({
    'post': 'create'
}, renderer_classes=[renderers.StaticHTMLRenderer])

bal = UserBalanceViewSet.as_view({
    'get': 'getbyname'
}, renderer_classes=[renderers.StaticHTMLRenderer])

userbalances = UserBalancesViewSet.as_view({
    'get': 'getuserbalances'
}, renderer_classes=[renderers.StaticHTMLRenderer])

send = TempTransactionsViewSet.as_view({
    'post': 'send'
}, renderer_classes=[renderers.StaticHTMLRenderer])

checking = TempTransactionsViewSet.as_view({
    'post': 'validate_add'
}, renderer_classes=[renderers.StaticHTMLRenderer])

profile_update = UserViewSet.as_view({
    'post': 'profile_update'
}, renderer_classes=[renderers.StaticHTMLRenderer])
getAddress = UserAddressViewSet.as_view({
    'get': 'getAddress'
}, renderer_classes=[renderers.StaticHTMLRenderer])
generateAddress = UserAddressViewSet.as_view({
    'get': 'generateAddress'
}, renderer_classes=[renderers.StaticHTMLRenderer])

buy = BuyViewSet.as_view({
    'post': 'buy'
}, renderer_classes=[renderers.StaticHTMLRenderer])

sell = SellViewSet.as_view({
    'post': 'sell'
}, renderer_classes=[renderers.StaticHTMLRenderer])

security = UserViewSet.as_view({
    'post': 'security'
}, renderer_classes=[renderers.StaticHTMLRenderer])

forgotReset = UserViewSet.as_view({
    'post': 'forgotReset'
}, renderer_classes=[renderers.StaticHTMLRenderer])

activate = UserViewSet.as_view({
    'post': 'activate'
}, renderer_classes=[renderers.StaticHTMLRenderer])

reset_activation = UserViewSet.as_view({
    'post': 'reset_activation'
}, renderer_classes=[renderers.StaticHTMLRenderer])

grecaptcha_verify = UserViewSet.as_view({
    'post': 'grecaptcha_verify'
}, renderer_classes=[renderers.StaticHTMLRenderer])

userDeposits = GetUserTransactions.as_view({
    'get': 'userDeposits'
}, renderer_classes=[renderers.StaticHTMLRenderer])

userWithdrawals = GetUserTransactions.as_view({
    'get': 'userWithdrawals'
}, renderer_classes=[renderers.StaticHTMLRenderer])

getCoinList = CoinList.as_view({
    'get': 'getCoinList'
}, renderer_classes=[renderers.StaticHTMLRenderer])

getBids = BidsAsksViewset.as_view({
    'get': 'getBids'
}, renderer_classes=[renderers.StaticHTMLRenderer])

getAsks = BidsAsksViewset.as_view({
    'get': 'getAsks'
}, renderer_classes=[renderers.StaticHTMLRenderer])

cron_view = cron.as_view({
    'get': 'widthdraw'
}, renderer_classes=[renderers.StaticHTMLRenderer])

notify = receive_wallet_notify.as_view({
    'post': 'wallet_notify'
}, renderer_classes=[renderers.StaticHTMLRenderer])


router = DefaultRouter()
router.register(prefix='user', viewset=UserViewSet)
router.register(prefix='useraddress', viewset=UserAddressViewSet)
router.register(prefix='coin', viewset=CoinViewSet)
router.register(prefix='getbalances', viewset=UserBalanceViewSet)
router.register(prefix='getuserbalances', viewset=UserBalancesViewSet)
router.register(prefix='send', viewset=TempTransactionsViewSet)
router.register(prefix='btctransactionsdetails', viewset=BtcTransactionDetailViewSet)
router.register(prefix='profile_update', viewset=UserViewSet)
router.register(prefix='buy', viewset=BuyViewSet)
router.register(prefix='sell', viewset=SellViewSet)


urlpatterns = format_suffix_patterns([

    url(r'^api/v1/login', obtain_jwt_token),
    url(r'^api/v1/verify', verify_jwt_token),
    url(r'^admin/', admin.site.urls),
    url(r'^api/v1/register/$', user, name='user'),
    url(r'^api/v1/username_available/$', UserViewSet.username_available, name='username_available'),
    url(r'^api/v1/useraddress/create/$', useraddress, name='useraddress'),
    url(r'^api/v1/coin/create/$', coin, name='coin'),
    url(r'^api/v1/getbalance/$', bal, name='getbalance'),
    url(r'^api/v1/getuserbalances/$', userbalances, name='getuserbalances'),
    url(r'^api/v1/send/$', send, name='send'),
    url(r'^api/v1/validate_add/$', checking, name='validate_add'),
    url(r'^api/v1/activate/$', activate, name='activate'),
    url(r'^api/v1/reset_activation/$', UserViewSet.reset_activation, name='reset_activation'),
    url(r'^api/v1/forgot/$', UserViewSet.forgot, name='forgot'),
    url(r'^api/v1/forgotReset/$', forgotReset, name='forgotReset'),
    url(r'^api/v1/security/$', security, name='security'),
    url(r'^api/v1/profile_update/$', profile_update, name='profile_update'),
    url(r'^api/v1/btc_conection_test/$', TestViewSet.btc_conection_test, name='btc_conection_test'),
    # url(r'^api/v1/login/$', CustomObtainJSONWebToken.testing, name='testing'),
    url(r'^api/v1/getAddress/$', getAddress, name='getAddress'),
    url(r'^api/v1/generateAddress/$', generateAddress, name='generateAddress'),
    url(r'^api/v1/buy/$', buy, name='buy'),
    url(r'^api/v1/sell/$', sell, name='sell'),
    url(r'^api/v1/getuserdeposits/$', userDeposits, name='userDeposits'),
    url(r'^api/v1/getuserwithdrawals/$', userWithdrawals, name='userWithdrawals'),
    url(r'^api/v1/getCoinList/$', getCoinList, name='getCoinList'),
    url(r'^api/v1/getBids/$', getBids, name='getBids'),
    url(r'^api/v1/getAsks/$', getAsks, name='getAsks'),
    url(r'^api/v1/cron/$', cron_view, name='widthdraw'),
    url(r'^api/v1/wallet_notify/$', notify, name='wallet_notify'),
])

urlpatterns += router.urls
