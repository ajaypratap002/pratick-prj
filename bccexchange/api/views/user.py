# Author : Astuto Lee (2018-01-12)
# Change whole page for constant message and code format and change JsonResponse format
import jwt,json
from django.contrib.auth.hashers import make_password, check_password
import requests
from django.utils.crypto import get_random_string
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse, JsonResponse
from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from api.models import User, user_balance, coins
from api.serializers import UserSerializer, UserBalanceSerializer, UserAddressSerializer, CoinSerializer
from django.conf import settings
from bccexchange.myconstants import MESSAGES  # import for use constants
from rest_framework_jwt.views import obtain_jwt_token, ObtainJSONWebToken
from django.contrib.auth import authenticate, login
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def register(self, request, *args, **kwargs):
        if request.method == "POST":
            try:
                serialized_user = UserSerializer(data=request.data)
                serialized_user.is_valid()
                request.data['status'] = 1
                unique_id = get_random_string(length=32)
                request.data['activation_hash'] = make_password(unique_id)
                serialized_user.create(request.data)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                try:
                    subject = MESSAGES.BCC_ACTIVATION_SUBJECT
                    to = [request.data['email']]
                    from_email = MESSAGES.BCC_EMAIL
                    ctx = {
                        'username': request.data['username'],
                        'email': request.data['email'],
                        'uniqueid': unique_id
                    }
                    htmly = get_template('userRegistraion.html')
                    html_content = htmly.render(ctx)
                    msg = EmailMultiAlternatives(subject, html_content, to=to, from_email=from_email)
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                except Exception:
                    return JsonResponse({"data": MESSAGES.REGISTERED_NO_EMAIL_SEND, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                else:
                    return JsonResponse({"data": MESSAGES.REGISTERED_EMAIL_SEND, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"message": request.method + MESSAGES.IS_NOT_VALID}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def username_available(request, *args, **kwargs):
        username = request.GET.get('username')
        sc = User.objects.filter(username=username)
        if sc.count() > 0:
            return JsonResponse({"data": MESSAGES.USERNAME_UNAVAILABLE, "available": False, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"data": MESSAGES.USERNAME_AVAILABLE, "available": True, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)

    @csrf_exempt
    def activate(self, request, *args, **kwargs):
        email = request.data['email']
        token = request.data['token']
        user_detail = User.objects.get(email=email)
        hash = user_detail.activation_hash
        if check_password(token, hash):
            user_detail.account_verified = 1
            user_detail.save()
            return JsonResponse({"data": MESSAGES.ACCOUNT_VERIFIED, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        else:
            return JsonResponse({"message": MESSAGES.ACCOUNT_NOT_VERIFICATION_FAIL}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @csrf_exempt
    def forgot(request, *args, **kwargs):
        if request.method == "POST":
            try:
                email = request.POST.get('email')
                try:
                    user_detail = User.objects.get(email=email)
                except User.DoesNotExist:
                    return JsonResponse({"message": MESSAGES.EMAIL_NOT_MATCH}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                unique_id = get_random_string(length=32)
                user_detail.reset_password_hash = make_password(unique_id)
                user_detail.save()
                subject = MESSAGES.RESET_PASSWORD_SUBJECT
                to = [email]
                from_email = MESSAGES.RESET_PASSWORD_EMAIL
                ctx = {
                    'username': user_detail.username,
                    'reset_link': MESSAGES.HOST + "/resetpassword?token=" + unique_id + "&email=" + email
                }
                htmly = get_template('forgotPassword.html')
                html_content = htmly.render(ctx)
                msg = EmailMultiAlternatives(subject, html_content, to=to, from_email=from_email)
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            except Exception:
                return JsonResponse({"message": MESSAGES.EMAIL_SEND_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return JsonResponse({"data": MESSAGES.RESET_PASSWORD_EMAIL_SEND + str(email) + ".", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)

    @csrf_exempt
    def forgotReset(self, request, *args, **kwargs):
        try:
            new_pass = request.data['new_password']
            confirm_password = request.data['confirm_password']
            email = request.data['email']
            token = request.data['token']
            user_detail = User.objects.get(email=email)
            if check_password(token, user_detail.reset_password_hash) is True:
                user_detail.password = make_password(new_pass)
                user_detail.save()
                return JsonResponse({"message": MESSAGES.PASSWORD_CHANGED}, status=status.HTTP_200_OK)
            else:
                return JsonResponse({"message": MESSAGES.PASSWORD_NOT_MATCH},
                                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            return JsonResponse({"message": str(e)},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @csrf_exempt
    def security(self, request, *args, **kwargs):
        # Author : astuto (2017-12-20)
        # created for change password
        if request.method == "POST":
            try:
                cur_pass = request.POST.get('cur_password')
                new_pass = request.POST.get('new_password')
                username = request.user.username
                user_detail = User.objects.get(username=username)
                hash = user_detail.password
                if check_password(cur_pass, hash) is True:
                    user_detail.password = make_password(new_pass)
                    user_detail.save()
                    email = user_detail.email
                    subject = MESSAGES.SECURITY_SUBJECT
                    to = [email]
                    from_email = MESSAGES.SECURITY_EMAIL
                    ctx = {
                        'email': email,
                        'username': user_detail.username,
                    }
                    htmly = get_template('change_pass.html')
                    html_content = htmly.render(ctx)
                    msg = EmailMultiAlternatives(subject, html_content, to=to, from_email=from_email)
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    return JsonResponse({"data": MESSAGES.PASSWORD_CHANGED, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                else:
                    return JsonResponse({"message": MESSAGES.PASSWORD_NOT_MATCH}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({"message": request.method + MESSAGES.IS_NOT_VALID}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @csrf_exempt
    def profile_update(self, request, *args, **kwargs):
        # Author :Rasmus Lerdorf(2017_12_20)
        # create code for user profile update
        if request.method == "POST":
            try:
                user_detail = User.objects.get(id=request.user.id)
                user_detail.firstname = request.POST.get('firstname')
                user_detail.lastname = request.POST.get('lastname')
                user_detail.mobile = request.POST.get('mobile')
                user_detail.country = request.POST.get('country')
                user_detail.zipcode = request.POST.get('zipcode')
                user_detail.save()
                return JsonResponse({"message": MESSAGES.PROFILE_UPDATED}, status=status.HTTP_200_OK)
            except Exception as e:
                return JsonResponse({"message": MESSAGES.PROFILE_UPDATED_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                # return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({"message": request.method + MESSAGES.IS_NOT_VALID}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @csrf_exempt
    def reset_activation(request, *args, **kwargs):
        if request.method == "POST":
            try:
                user_detail = User.objects.get(email=request.POST.get('email'))
                if user_detail.account_verified == 1:
                    return JsonResponse({"message": MESSAGES.ACCOUNT_ALREADY_VERIFIED}, status=status.HTTP_200_OK)
                else:
                    try:
                        subject = MESSAGES.BCC_ACTIVATION_SUBJECT
                        to = [request.POST.get('email')]
                        from_email = MESSAGES.BCC_EMAIL
                        unique_id = get_random_string(length=32)
                        ctx = {
                            'username': user_detail.username,
                            'email': request.POST.get('email'),
                            'uniqueid': unique_id
                        }
                        htmly = get_template('userRegistraion.html')
                        html_content = htmly.render(ctx)
                        msg = EmailMultiAlternatives(subject, html_content, to=to, from_email=from_email)
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        user_detail.activation_hash = make_password(unique_id)
                        user_detail.save()
                    except Exception as e:
                        return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                    else:
                        return JsonResponse({"message": "resend activation mail has been send"}, status=status.HTTP_200_OK)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    # @csrf_exempt
    # def grecaptcha_verify(self, request, *args, **kwargs):
    #         response = {}
    #         captcha_rs = request.data['recaptcha']
    #         if not captcha_rs:
    #             return JsonResponse({"message": "Please verify valid captcha"}, status=status.HTTP_200_OK)
    #         else:
    #             url = "https://www.google.com/recaptcha/api/siteverify"
    #             params = {
    #                 'secret': settings.RECAPTCHA_PRIVATE_KEY,
    #                 'response': captcha_rs
    #             }
    #             verify_rs = requests.get(url, params=params, verify=True)
    #             verify_rs = verify_rs.json()
    #             response["status"] = verify_rs.get("success", False)
    #             if response["status"] is False:
    #                 return JsonResponse({"message": "Please verify valid captcha"}, status=status.HTTP_200_OK)
    #             else:
    #                 return obtain_jwt_token(self)
    #

# class CaptchaJWT(ObtainJSONWebToken):
#     @csrf_exempt
#     def post(request):
#         response = super().post(request)
#         # foo bar
#         return response

class UserBalanceViewSet(viewsets.ModelViewSet):
    queryset = user_balance.objects.all()
    serializer_class = UserBalanceSerializer
    permission_classes = (IsAuthenticated,)

    def getbyname(self, request, *args, **kwargs):
        data = request.GET.get("name")
        currency = format(data)
        single_user_bal_detail_qry = user_balance.objects.filter(user_id=request.user.id, coin__abbreviation=currency).select_related()
        single_user_bal_detail_serializer = UserBalanceSerializer(single_user_bal_detail_qry, many=True)
        try:
            return JsonResponse({"data": single_user_bal_detail_serializer.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.DISPLAY_BALANCE_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserBalancesViewSet(viewsets.ModelViewSet):
    queryset = user_balance.objects.all()
    serializer_class = UserBalanceSerializer
    permission_classes = (IsAuthenticated,)

    def getuserbalances(self, request, *args, **kwargs):
        try:
            get_all_coin_by_uid = user_balance.objects.filter(user_id=request.user.id)
            get_all_coin_by_uid_serializer = UserBalanceSerializer(get_all_coin_by_uid, many=True)
            return JsonResponse({"data": get_all_coin_by_uid_serializer.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.DISPLAY_BALANCE_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
