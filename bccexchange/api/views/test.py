from django.http import HttpResponse, JsonResponse
from bitcoinrpc.authproxy import AuthServiceProxy
from rest_framework import viewsets
from bccexchange.lib.wallet import Wallet
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser


class TestViewSet(viewsets.ModelViewSet):

    @csrf_exempt
    def btc_conection_test(request):
        w = Wallet(1)
        return JsonResponse({"message": w.getinfo()})
        # rpc_connection = AuthServiceProxy("http://Harry03BC12:PaHD12YDHaai7621234@128.199.166.25:3032")
        # best_block_hash = rpc_connection.getbestblockhash()
        # return JsonResponse({"message":rpc_connection.getinfo()})
        # print(rpc_connection.getinfo())


# class APILoginViewSet(APIView):
#     """
#       Returns user info and tokens if successful
#       """
#     @csrf_exempt
#     def grecaptcha_verify(request):
#         """
#         Secondary login method. Uses email and password
#         Recommendation: Use `POST:/api/v1/auth/api-jwt-auth/` instead
#         """
#         # data = JSONParser().parse(request)
#         # data.get('recaptcha')
#         # print(data)
#         # return JsonResponse({"message": data.get('recaptcha')}, status=status.HTTP_200_OK)
#         # serializer = LoginCustomSerializer(data=data)
#         #
#         # if serializer.is_valid():
#         #     email = serializer.data.get('email')
#         #     password = serializer.data.get('password')
#         #
#         #     if not request.user.is_anonymous:
#         #         return Response('Already Logged-in', status=status.HTTP_403_FORBIDDEN)
#         #
#         #     user = authenticate(email=email, password=password)
#         #
#         #     if user is not None:
#         #         if user.is_active:
#         #             login(request, user)
#         #
#         #             serialized = UserSerializer(user)
#         #             data = serialized.data
#         #
#         #             # Also return JWT token
#         #             data['jwt'] = get_jwt_token(user)
#         #
#         #             return Response(data)
#         #         else:
#         #             return Response('This user is not Active.', status=status.HTTP_401_UNAUTHORIZED)
#         #     else:
#         #         return Response('Username/password combination invalid.', status=status.HTTP_401_UNAUTHORIZED)
#         #
#         # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
