# Author : Astuto Lee (2018-01-12)
# Change page for constant message and code format and change JsonResponse format
import json
import sys
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from rest_framework import viewsets, status
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from rest_framework.permissions import IsAuthenticated,AllowAny
from api.models import user_address, coins, user_balance, btc_temp_transactions, btc_transaction, btc_transaction_detail
from api.serializers import UserAddressSerializer, CoinSerializer, UserBalanceSerializer, TempTransactionsSerializer, btcTransactionsSerializer, btcTransactionsDetailSerializer
from bccexchange.myconstants import MESSAGES # import for use constants

class CoinViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Employee objects  """
    queryset = coins.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = CoinSerializer


    def create(self, request):
        serializer = CoinSerializer(data=request.data)
        if serializer.is_valid():
            try:
                serializer.save()
                return JsonResponse({"data": request.data['abbreviation'] + MESSAGES.CREATED_SUCCESS, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
            except Exception:
                return JsonResponse({"message": MESSAGES.COIN_CREAT_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return HttpResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CoinList(viewsets.ModelViewSet):
    # Author: Astuto Lee(2017 - 12 - 27)
    # Get Coin List
    queryset = coins.objects.all()
    permission_classes = (IsAuthenticated, )
    serializer_class = CoinSerializer

    def getCoinList(self, request, *args, **kwargs):
        try:
            serialized = CoinSerializer(coins.objects.all(), many=True)
            return JsonResponse({"data": serialized.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.COIN_LIST_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)