# Author :Rasmus Lerdorf(2017_12_20)
# create code buy and sell
# Author : Astuto Lee (2018-01-12)
# Change page for constant message and code format and change JsonResponse format
from django.db.models import Case, When, Value, CharField
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from api.models import buy, sell, User
from api.serializers import buySerializer, sellSerializer, UserSerializer
from django.template.loader import get_template
from bccexchange.myconstants import MESSAGES # import for use constants

class BuyViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = buy.objects.all()
    serializer_class = buySerializer

    def buy(self, request, *args, **kwargs):
        if request.method == "POST":
            try:
                request.POST._mutable = True
                request.POST['user'] = request.user.id       
                serialized_buy = buySerializer(data=request.data)            
                serialized_buy.is_valid()            
                serialized_buy.save()
                return JsonResponse({"data": MESSAGES.REQUEST_SEND, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({"message": request.method + MESSAGES.IS_NOT_VALID}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class SellViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = sell.objects.all()
    serializer_class = sellSerializer

    def sell(self, request, *args, **kwargs):
        if request.method == "POST":
            try:
                request.POST._mutable = True
                request.POST['user'] = request.user.id       
                serialized_sell = sellSerializer(data=request.data)            
                serialized_sell.is_valid()            
                serialized_sell.save()
                return JsonResponse({"data": MESSAGES.REQUEST_SEND, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({"message": request.method + MESSAGES.IS_NOT_VALID}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class BidsAsksViewset(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = buy.objects.all()
    ask_queryset = sell.objects.all()
    serializer_class = buySerializer, sellSerializer

    def getBids(self, request, *args, **kwargs):
        try:
            coin_id = request.GET.get('coin_id')
            user_id = request.user.id
            bids_detail = buy.objects.filter(user_id=user_id)
            bids_detail = buy.objects.annotate(
                user_id=Case(
                    When(user_id=user_id, then=Value('1')),
                    default=Value(''),
                    output_field=CharField(),
                ), )
            # bids_detail = buy.objects.all()
            # if user_id:
            #     bids_detail = bids_detail.filter(user_id=user_id)
            bids_detail_serialized = buySerializer(bids_detail, many=True)
            # bids_detail_serialized.new = 'a'
            # bids_detail_serialized.save()
            return JsonResponse({"data": bids_detail_serialized.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def getAsks(self, request, *args, **kwargs):
        try:
            coin_id = request.GET.get('coin_id')
            user_id = request.user.id
            ask_detail = sell.objects.filter(user_id=user_id)
            # ask_detail = sell.objects.all()
            ask_detail = sell.objects.annotate(
                user_id=Case(
                    When(user_id=user_id, then=Value('1')),
                    default=Value(''),
                    output_field=CharField(),
                ), )
            ask_detail_serialized = sellSerializer(ask_detail, many=True)
            return JsonResponse({"data": ask_detail_serialized.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
