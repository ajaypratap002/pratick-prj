# Author : Astuto Lee (2018-01-12)
# Change page for constant message and code format and change JsonResponse format
from api.models import user_address
from api.serializers import UserAddressSerializer
from bccexchange.lib.wallet import Wallet
from bccexchange.myconstants import MESSAGES  # import for use constants
from django.http import JsonResponse
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated


class UserAddressViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Employee objects """
    queryset = user_address.objects.all()
    serializer_class = UserAddressSerializer
    permission_classes = (IsAuthenticated,)

    def getAddress(self, request, *args, **kwargs):
        try:
            if request.user.is_banned:
                return JsonResponse({"message": MESSAGES.OPERATION_NOT_ALLOW}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                user_address_queryset = user_address.objects.filter(user_id=request.user.id, coin_id=request.GET.get('coin_id')).select_related()
                user_address_details = UserAddressSerializer(user_address_queryset, many=True)
                if user_address_queryset.count() > 0:
                    return JsonResponse({"address": user_address_details.data[0]['address'], "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                else:
                    return JsonResponse({"address": ''}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.ADDRESS_CHECK_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def generateAddress(self, request, *args, **kwargs):
        try:
            if request.user.is_banned:
                return JsonResponse({"message": MESSAGES.OPERATION_NOT_ALLOW}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                w = Wallet(request.GET.get('coin_id'))
                new_user_address = w.getnewaddress(request.user.username)
                ud = user_address(address=new_user_address, coin_id=request.GET.get('coin_id'), user_id=request.user.id, status='1')
                ud.save()
                return JsonResponse({"address": new_user_address, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.NEW_ADDRESS_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
