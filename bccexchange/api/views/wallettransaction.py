# Author : Ajayyadav (2018-01-12)
# Change page for constant message and code format and change JsonResponse format
import sys
import time
from datetime import datetime

from api.models import User, coins, user_balance, btc_temp_transactions, btc_transaction, btc_transaction_detail, btc_widthdraw_error
from api.serializers import UserBalanceSerializer, TempTransactionsSerializer, btcTransactionsSerializer, btcTransactionsDetailSerializer
from bccexchange.lib.wallet import Wallet
from bccexchange.myconstants import MESSAGES  # import for use constants
from dateutil import relativedelta
from django.contrib.auth.hashers import check_password
from django.core.mail import EmailMultiAlternatives
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.http import JsonResponse
from django.template.loader import get_template
from django.utils.crypto import get_random_string
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from api.validate_address import add_valid



class TransactionsViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Employee objects """
    queryset = btc_temp_transactions.objects.all()
    serializer_class = TempTransactionsSerializer
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def send(self, request, *args, **kwargs):
        if request.method == "POST":
            if request.user.is_banned:
                return JsonResponse({"message": MESSAGES.OPERATION_NOT_ALLOW}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                if getattr(add_valid, "check_" + request.data['currency'].lower())(request.data['to_address']) is True:
                    if check_password(request.data['password'], request.user.password) is True:
                        try:
                            username = request.user.username
                            currency = request.data['currency']
                            tx_fee = request.data['txfee']
                            amt = request.data['amount']
                            tot_updt_bal = round(float(tx_fee), 8) + round(float(amt), 8)
                            coin_id = coins.objects.get(abbreviation=currency)
                            user_bal = user_balance.objects.get(user_id=request.user.id, coin_id=coin_id.id)
                            if tot_updt_bal > user_bal.available_balance:
                                return JsonResponse({"message": MESSAGES.INSUFFICIENT_BALANCE + str(currency) + "."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                            else:
                                with transaction.atomic():
                                    current = user_balance.objects.select_for_update().get(user_id=request.user.id, coin_id=coin_id.id)
                                    current.available_balance = round(float(current.available_balance), 8) - round(float(tot_updt_bal), 8)
                                    current.save()
                                    temp_trans = getattr(sys.modules[__name__], currency.lower() + "_temp_transactions")\
                                        (user_id=request.user.id, to_address=request.data['to_address'], amount=round(float(request.data['amount']), 8), tx_fee=tx_fee, status='0')
                                    temp_trans.save()
                        except Exception:
                            return JsonResponse({"message": MESSAGES.SERVICE_UNAVAILABLE}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                        else:
                            try:
                                subject = MESSAGES.TRANSACTION_SEND_SUBJECT
                                to = [request.user.email]
                                from_email = MESSAGES.TRANSACTION_SEND_EMAIL
                                ctx = {
                                    'username': username,
                                    'address': request.data['to_address'],
                                    'amount': request.data['amount'],
                                    'currency': request.data['currency']
                                }
                                htmly = get_template('sendUserBal.html')
                                html_content = htmly.render(ctx)
                                msg = EmailMultiAlternatives(subject, html_content, to=to, from_email=from_email)
                                msg.attach_alternative(html_content, "text/html")
                                msg.send()
                            except Exception:
                                return JsonResponse({"data": MESSAGES.TRANSACTION_SEND_EMAIL_NOT_SEND, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                            else:
                                return JsonResponse({"data": MESSAGES.SEND_SUCCESS + request.data['amount'] + " " + request.data['currency'] + ".", "message": MESSAGES.SUCCESS_MSG},
                                                    status=status.HTTP_200_OK)
                    else:
                        return JsonResponse({"message": MESSAGES.PASSWORD_NOT_MATCH}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    return JsonResponse({"message": MESSAGES.ADDRESS_NOT_MATCH + " " + request.data['currency'] + " " + "Address."}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# Receive Wallet Notification function
class receive_wallet_notify(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)

    def wallet_notify(self, request, *args, **kwargs):
        if request.POST.get('currency') and request.POST.get('transaction_id'):
            try:
                coin_data = coins.objects.get(abbreviation=request.POST.get('currency').upper())
                coin_id = coin_data.id
                tx_fee = coin_data.tx_fee
                user_id = request.user.id
                transaction_id = request.POST.get('transaction_id')
                w = Wallet(coin_id)
                transaction_data = w.gettransaction(transaction_id)
                confirmations = transaction_data['confirmations']
                details = transaction_data['details']
                btc_transaction_data = btc_transaction.objects.filter(transaction_id=transaction_id)
                btc_transaction_serialized_data = btcTransactionsSerializer(btc_transaction_data, many=True)
                already_exists_transaction = []
                for i in range(len(btc_transaction_serialized_data.data)):
                    already_exists_transaction = btc_transaction_serialized_data.data[i]['id']
                if transaction_data or transaction_data['details']:
                    if details:
                        if not already_exists_transaction:
                            cur_dt = datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                            old_dt = datetime.fromtimestamp(transaction_data['time']).strftime("%Y-%m-%d %H:%M:%S")
                            date1 = datetime.strptime(cur_dt, '%Y-%m-%d %H:%M:%S')
                            date2 = datetime.strptime(old_dt, '%Y-%m-%d %H:%M:%S')
                            r = relativedelta.relativedelta(date2, date1)
                            month_difference = r.months
                            if month_difference <= 1:
                                try:
                                    btc_transaction_data = btc_transaction(transaction_id=transaction_data['txid'], amount=round(float(transaction_data['amount']), 8), tx_fee=None,
                                                                           confirmations=transaction_data['confirmations'], comment=None)
                                    btc_transaction_data.save()
                                    return JsonResponse({"data": MESSAGES.INSERT_TRANSACTION_TBL, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                except Exception as e:
                                    return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                            else:
                                return JsonResponse({"data": MESSAGES.OLD_TRANSACTION, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                        else:
                            insert_id = already_exists_transaction
                        self.insert_transaction_detail(user_id, transaction_id, details, tx_fee)
                        if confirmations > 0 and insert_id:
                            btc_transaction.objects.filter(id=insert_id).update(confirmations=confirmations)
                            transaction_detail_qry = btc_transaction_detail.objects.filter(transaction_id=transaction_id, category=1, is_balance_updated=0)
                            transaction_detail_data = btcTransactionsDetailSerializer(transaction_detail_qry, many=True)
                            if transaction_detail_qry.count() > 0:
                                for i in range(len(transaction_detail_data.data)):
                                    try:
                                        with transaction.atomic():
                                            user_balance_data = user_balance.objects.filter(user_id=transaction_detail_data.data[i]['user_id'])
                                            user_balance_serializer_data = UserBalanceSerializer(user_balance_data, many=True)
                                            for j in range(len(user_balance_serializer_data.data)):
                                                total_amount = round(user_balance_serializer_data.data[j]['available_balance'] + round(transaction_detail_data.data[i]['amount'], 8), 8)
                                                user_balance.objects.filter(id=user_balance_serializer_data.data[j]['id']).update(available_balance=total_amount)
                                            if transaction_detail_data.data[i]['user_id']:
                                                user_balance.objects.filter(user_id=transaction_detail_data.data[i]['user_id']).update(coin_id=coin_id,
                                                                                                                                       modified_by=transaction_detail_data.data[i]['user_id'])
                                                btc_transaction_detail.objects.filter(id=transaction_detail_data.data[i]['id'], is_balance_updated=0, category=1).update(is_balance_updated=1)
                                                return JsonResponse({"data": MESSAGES.USER_BALANCE_UPDATE, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                            else:
                                                return JsonResponse({"data": MESSAGES.ERROR_IN_INSERT, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                    except Exception as e:
                                        withdraw_error_details = btc_widthdraw_error(user_id=transaction_detail_data.data[i]['user_id'], custom_detail='During Received ' + str(
                                            coin_data.abbreviation) + ' does not update confirmed real balance transaction detail ID : ' + str(transaction_detail_data.data[i]['id']))
                                        withdraw_error_details.save()
                                        return JsonResponse({"message": str(e) + " transaction"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                            else:
                                return JsonResponse({"data": "transaction detail is not found because is_update_balance is 1", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                        else:
                            return JsonResponse({"data": "Confirmation or transactions problem", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                    else:
                        try:
                            for i in range(len(already_exists_transaction)):
                                btc_transaction.objects.filter(id=already_exists_transaction[i]['id']).update(deleted=1)
                                btc_transaction_detail.objects.filter(id=already_exists_transaction[i]['id']).update(deleted=1)
                                withdraw_error_details = btc_widthdraw_error(user_id=1,
                                                                             custom_detail='Double spend ' + str(coin_data.abbreviation) + ' txid : ' + str(already_exists_transaction[i]['id']))
                                withdraw_error_details.save()
                                return JsonResponse({"data": "Detail is empty, data updated in transaction, transaction_details, error table", "message": MESSAGES.SUCCESS_MSG},
                                                    status=status.HTTP_200_OK)
                        except Exception as e:
                            return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    return JsonResponse({"data": "Error while fetching transaction data", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
            except Exception as e:
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return JsonResponse({"message": "Please provide required currency, transaction_id"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def insert_transaction_detail(self, user_id, transaction_id, details, tx_fee):
        try:
            for i in range(len(details)):
                if details[i]['category'] == 'receive':
                    category = 1
                else:
                    category = 2
                already_added_transaction = btc_transaction_detail.objects.filter(transaction_id=transaction_id, category=category, address=details[i]['address'])
                if not already_added_transaction:
                    try:
                        btc_transaction_detail_data = btc_transaction_detail(user_id=user_id, transaction_id=transaction_id, address=details[i]['address'], amount=round(details[i]['amount'], 8),
                                                                             tx_fee=tx_fee, category=category, is_balance_updated=0)
                        btc_transaction_detail_data.save()
                        return JsonResponse({"data": MESSAGES.INSERT_SUCCESS, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                    except Exception as e:
                        return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    return JsonResponse({"data": MESSAGES.INSERT_ALREADY_EXISTS, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class cron(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = btc_temp_transactions.objects.all()
    serializer_class = TempTransactionsSerializer

    def widthdraw(self, request, *args, **kwargs):
        coin_id = 1
        w = Wallet(coin_id)
        transaction.set_autocommit(False)
        try:
            transaction_detail_data = btc_temp_transactions.objects.select_for_update().filter(user_id=request.user.id)
            if transaction_detail_data.count() > 0:
                serialized_data = TempTransactionsSerializer(transaction_detail_data, many=True)
                unique_address_send_to_final_data = {}
                ids = {}
                for i in range(len(serialized_data.data)):
                    if serialized_data.data[i]['to_address'] not in unique_address_send_to_final_data:
                        unique_address_send_to_final_data[serialized_data.data[i]['to_address']] = []
                        unique_address_send_to_final_data[serialized_data.data[i]['to_address']] = serialized_data.data[i]['amount']
                        ids[i] = serialized_data.data[i]['id']
                total_count = len(ids)
                broadcast_id = get_random_string(length=16)
                ids_string = []
                for value in ids:
                    ids_string.append(ids[value])
                row = btc_temp_transactions.objects.filter(id__in=ids_string).update(status=1, broadcast_id=broadcast_id)
                if row > 0:
                    if row == total_count:
                        try:
                            transaction.commit()
                            send_response_transaction_id = w.sendmany(unique_address_send_to_final_data, 1, broadcast_id)
                            if send_response_transaction_id['error']:
                                send_response = send_response_transaction_id['error'][0:28]
                                if send_response == 'Invalid bitconnect address: ':
                                    invalid_address = send_response_transaction_id['error'][28:]
                                    for i in range(len(serialized_data.data)):
                                        if serialized_data.data[i]['to_address'] == invalid_address:
                                            problem_id = serialized_data.data[i]['id']
                                    btc_temp_transactions.objects.filter(id=problem_id).update(status=2)
                                    return JsonResponse({"data": MESSAGES.UPDATE_INVALID_ADDRESS_STATUS, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                else:
                                    btc_temp_transactions.objects.filter(id__in=ids_string).update(status=2)
                                    try:
                                        withdraw_error_details = btc_widthdraw_error(broadcast_id=broadcast_id, error=send_response_transaction_id['error'], custom_detail='send BCC error')
                                        withdraw_error_details.save()
                                        return JsonResponse({"data": MESSAGES.INSERT_ERROR_TBL, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                    except Exception as e:
                                        return JsonResponse({"message": e.message}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                            else:
                                return JsonResponse({"data": "Else part", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                                send_success = True
                                transaction_data = w.gettransaction(send_response_transaction_id)
                                if transaction_data['error'] and transaction_data and transaction_data['details']:
                                    tx_fee_value = round(float(transaction_data['fee']), 8) if transaction_data['fee'] else None
                                    confirmation_value = transaction_data['confirmations'] if transaction_data['confirmations'] else 0
                                    comment_value = transaction_data['comment'] if round(float(transaction_data['comment']), 8) else None
                                    btc_transaction_data = btc_transaction(transaction_id=send_response_transaction_id, amount=round(float(transaction_data['amount']), 8), tx_fee=tx_fee_value, confirmations=confirmation_value, comment=comment_value, broadcast_id=broadcast_id)
                                    transaction_id = btc_transaction_data.save()
                                    btc_temp_transactions_data = btc_temp_transactions.objects.filter(broadcast_id=broadcast_id)
                                    temp_transaction_data = TempTransactionsSerializer(btc_temp_transactions_data, many=True)
                                    for temp_transaction_data.data in row:
                                        btc_transaction_detail_data = btc_transaction_detail(btc_transaction_id=transaction_id, address=row.to_address, user_id=row.user_id, amount=round(float(row.amount), 8), tx_fee=round(float(row.tx_fee), 8) if row.tx_fee else None, category=2, vout=transaction_data['vin'][0]['vout'], is_balance_updated=1)
                                        btc_transaction_detail_data.save()
                                else:
                                    withdraw_error_details = btc_widthdraw_error(broadcast_id=broadcast_id,
                                                                              error=transaction_data['error'],
                                                                              custom_detail='after send BCC got transaction error')
                                    withdraw_error_details.save()
                        except Exception as e:
                            return JsonResponse({"data": str(e.message), "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                    else:
                        return JsonResponse({"data": MESSAGES.TOTAL_ROW_NOT_MATCH, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
                        # transaction.rollback()
                else:
                    return JsonResponse({"data": "Error while updating", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
            else:
                return JsonResponse({"data": "No data found", "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception as e:
            if send_success is True:
                withdraw_error_details = btc_widthdraw_error(broadcast_id=broadcast_id,
                                                          custom_detail='after send BCC update error catch',
                                                          transaction_hash=send_response_transaction_id)
                withdraw_error_details.save()
                return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            transaction.set_autocommit(True)

class BtcTransactionDetailViewSet(viewsets.ModelViewSet):
    """ ViewSet for viewing and editing Employee objects """
    queryset = btc_transaction_detail.objects.all()
    serializer_class = btcTransactionsDetailSerializer

    def btcTransactions(self, request, *args, **kwargs):
        queryset = btc_transaction_detail.objects.raw(
            'SELECT api_btc_transaction_detail.* FROM api_btc_transaction_detail INNER JOIN api_btc_transaction ON api_btc_transaction_detail.btc_Transaction_id=api_btc_transaction.id ')
        serializer = btcTransactionsSerializer(queryset, many=True)
        try:
            return JsonResponse({"data": serializer.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.TRANSACTION_DETAIL_DISPLAY_ERROR}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GetUserTransactions(viewsets.ModelViewSet):
    # Author : Astuto Lee (2017-12-21)
    # Get user's Deposit Transactions
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = btcTransactionsDetailSerializer

    def default(self, o):
        try:
            iterable = iter(o)
        except TypeError:
            pass
        else:
            return list(iterable)
        # Let the base class default method raise the TypeError
        return DjangoJSONEncoder.default(self, o)

    def userDeposits(self, request, *args, **kwargs):
        # user_detail = User.objects.get(id=request.user.id)
        coin_id = request.GET.get("coin_id")
        coin_details = coins.objects.get(id=coin_id)
        coin_name = coin_details.abbreviation.lower()
        field_name = coin_name + "_Transaction__id"
        transaction_tbl = getattr(sys.modules[__name__], coin_name + "_transaction")
        transaction_detail_tbl = getattr(sys.modules[__name__], coin_name + "_transaction_detail")
        transaction_serializer = getattr(sys.modules[__name__], coin_name + "TransactionsDetailSerializer")
        try:
            # transaction_detail_data = temp_tbl.objects.filter(user_id=request.user.id, category=1)
            transaction_detail_data = transaction_detail_tbl.objects.filter(user_id=request.user.id, category=1).select_related().values()
            # transaction_detail_data = temp_tbl.objects.all().values('category').annotate(n=F('category'))
            # serialized_data = btcTransactionsDetailSerializer(transaction_detail_data, many=True) # original for only btc
            serialized_data = transaction_serializer(transaction_detail_data, many=True)
            # return JsonResponse({"data": transaction_detail_data[0].amount}, status=status.HTTP_200_OK)
            return JsonResponse({"data": serialized_data.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        # except ObjectDoesNotExist as error:
        #     return JsonResponse({"message": "No Data"}, status=status.HTTP_200_OK)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            # return JsonResponse({"message": "Something wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def userWithdrawals(self, request, *args, **kwargs):
        coin_id = request.GET.get("coin_id")
        try:
            transaction_detail_data = btc_transaction_detail.objects.only('category').filter(user_id=request.user.id, category=2)
            serialized_data = btcTransactionsDetailSerializer(transaction_detail_data, many=True)
            return JsonResponse({"data": serialized_data.data, "message": MESSAGES.SUCCESS_MSG}, status=status.HTTP_200_OK)
        except Exception:
            return JsonResponse({"message": MESSAGES.SOMETHING_WRONG}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

