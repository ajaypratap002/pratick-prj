# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-13 05:18
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0015_btc_transaction_deleted'),
    ]

    operations = [
        migrations.CreateModel(
            name='ltc_temp_transactions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_id', models.SmallIntegerField()),
                ('to_address', models.CharField(max_length=400)),
                ('amount', models.FloatField()),
                ('tx_fee', models.FloatField()),
                ('status', models.SmallIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='ltc_transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transaction_id', models.CharField(max_length=400)),
                ('amount', models.FloatField()),
                ('tx_fee', models.FloatField()),
                ('confirmations', models.SmallIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.CreateModel(
            name='ltc_transaction_detail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.SmallIntegerField()),
                ('user_id', models.SmallIntegerField()),
                ('address', models.CharField(max_length=400)),
                ('amount', models.FloatField()),
                ('tx_fee', models.FloatField()),
                ('vout', models.IntegerField()),
                ('is_balance_updated', models.SmallIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('ltc_Transaction', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ltc_transaction_id_fk', to='api.ltc_transaction')),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
        migrations.AlterUniqueTogether(
            name='user_balance',
            unique_together=set([('coin', 'user_id')]),
        ),
    ]
