from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from .models import User

class UserSerializer(serializers.ModelSerializer):
    # email = serializers.EmailField(
    #         required=True,
    #         validators=[UniqueValidator(queryset=User.objects.all())]
    #         )
    # username = serializers.CharField(
    #         validators=[UniqueValidator(queryset=User.objects.all())]
    #         )
    # password = serializers.CharField(min_length=8)
    # firstname= serializers.CharField(required=True)
    # lastname= serializers.CharField(required=True)
    # mobile= serializers.CharField(required=True)
    # country= serializers.CharField(required=True)
    class Meta:
        model = User
        fields = ('id', 'username','password', 'email','firstname','lastname','mobile','country','zipcode','country_code','status','account_verified','date_created','date_modified','is_admin')

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

from rest_framework import serializers
from .models import user_address, coins, user_balance, btc_temp_transactions, btc_transaction, btc_transaction_detail, buy, sell, ltc_temp_transactions, ltc_transaction, ltc_transaction_detail


class CoinSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    class Meta:
        model = coins
        fields = ("id", "coin_name", "abbreviation", "tx_fee", "created", "last_updated")

class UserAddressSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    class Meta:
        model = user_address
        fields = ("id", "address", "coin_id", "status", "user_id", "created", "last_updated")

class UserBalanceSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    coin = CoinSerializer(read_only=True)
    class Meta:
        model = user_balance
        fields = ("id", "coin", "user_id", "unconfirmed_balance", "escrow", "available_balance", "created", "last_updated")

class TempTransactionsSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    class Meta:
        model = btc_temp_transactions
        fields = ("id", "user_id", "to_address", "amount", "tx_fee", "status", "created", "last_updated")
        depth = 1

class ltcTempTransactionsSerializer(serializers.ModelSerializer):
    # Author : Astuto Lee (2017-12-28)
    # litecoin table serialize
    class Meta:
        model = ltc_temp_transactions
        fields = ("user_id", "to_address", "amount", "tx_fee", "status", "created", "last_updated")
        depth = 1

class btcTransactionsSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    class Meta:
        model = btc_transaction
        fields = ("id", "transaction_id", "amount", "tx_fee", "confirmations", "created", "last_updated")
        depth = 1

class ltcTransactionsSerializer(serializers.ModelSerializer):
    # Author : Astuto Lee (2017-12-28)
    # litecoin table serialize
    class Meta:
        model = ltc_transaction
        fields = ("transaction_id", "amount", "tx_fee", "confirmations", "created", "last_updated")
        depth = 1

class btcTransactionsDetailSerializer(serializers.ModelSerializer):
    """ Serializer to represent the Employee model """
    btc_Transaction = btcTransactionsSerializer(read_only=True)
    class Meta:
        model = btc_transaction_detail
        fields = ("id", "btc_Transaction", "category", "user_id", "address", "amount", "tx_fee", "vout", "is_balance_updated", "created", "last_updated")
        depth = 1

class ltcTransactionsDetailSerializer(serializers.ModelSerializer):
    # Author : Astuto Lee (2017-12-28)
    # litecoin table serialize
    ltc_Transaction=ltcTransactionsSerializer(read_only=True)
    class Meta:
        model = ltc_transaction_detail
        fields = ("ltc_Transaction", "category", "user_id", "address", "amount", "tx_fee", "vout", "is_balance_updated", "created", "last_updated")
        depth = 1

class buySerializer(serializers.ModelSerializer):     
    """ Serializer to represent the Employee model """   
    class Meta:
        model = buy
        fields = ("id", "units", "remain_buy_units", "bid", "fees", "total", "status", "user")
        

class sellSerializer(serializers.ModelSerializer):     
    """ Serializer to represent the Employee model """   
    class Meta:
        model = sell
        fields = ("id", "units", "remain_buy_units", "ask", "fees", "total", "status", "user")
