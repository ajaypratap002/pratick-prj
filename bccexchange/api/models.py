from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.urls import reverse


class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(email=email, username=username, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    username = models.CharField(unique=True, max_length=50)
    email = models.EmailField(unique=True)
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15)
    country = models.CharField(max_length=20)
    zipcode = models.CharField(max_length=12, blank=True)
    country_code = models.CharField(max_length=12, blank=True)
    status = models.SmallIntegerField(null=True)
    account_verified = models.BooleanField(default=False)
    activation_hash = models.CharField(max_length=100, blank=True)
    reset_password_hash = models.CharField(max_length=100, blank=True)
    is_banned = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=None)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def get_full_name(self):
        return ' '.join(self.firstname, self.lastname)

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class coins(models.Model):
    # Fields
    coin_name = models.CharField(max_length=30)
    abbreviation = models.CharField(max_length=10, unique=True)
    tx_fee = models.FloatField(default=None)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    wallet_connection = models.CharField(max_length=300)
    crypto_type = models.CharField(max_length=30)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('coins_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('coins_update', args=(self.pk,))


class user_address(models.Model):
    # Fields
    address = models.TextField(max_length=40, default=None)
    coin = models.ForeignKey(coins, related_name='user_add_coin_fk', default=None)
    user = models.ForeignKey(User, related_name='user_id_fk', default=None)
    status = models.CharField(max_length=8, default=None)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('user_address_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('user_address_update', args=(self.pk,))


class user_balance(models.Model):
    # Fields
    coin = models.ForeignKey(coins, related_name='coin_id_fk', null=True)
    user_id = models.SmallIntegerField(null=True)
    unconfirmed_balance = models.FloatField(null=True)
    escrow = models.FloatField(null=True)
    available_balance = models.FloatField(null=True)
    modified_by = models.SmallIntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)
        unique_together = ('coin', 'user_id',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('app_name_user_balance_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('app_name_user_balance_update', args=(self.pk,))


class btc_temp_transactions(models.Model):
    # Fields
    user_id = models.SmallIntegerField(null=True)
    to_address = models.CharField(max_length=400, null=True)
    amount = models.FloatField(null=True)
    tx_fee = models.FloatField(null=True)
    status = models.SmallIntegerField(null=True)
    broadcast_id = models.CharField(max_length=16, null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('btc_temp_transactions_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('btc_temp_transactions_update', args=(self.pk,))

class ltc_temp_transactions(models.Model):
    # Author : Astuto Lee (2017-12-28)
    # table for lite coin
    user_id = models.SmallIntegerField()
    to_address = models.CharField(max_length=400)
    amount = models.FloatField()
    tx_fee = models.FloatField()
    status = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('ltc_temp_transactions_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('ltc_temp_transactions_update', args=(self.pk,))

class eth_temp_transactions(models.Model):
    # Fields
    user_id = models.SmallIntegerField()
    to_address = models.CharField(max_length=400)
    amount = models.FloatField()
    tx_fee = models.FloatField()
    status = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('btc_temp_transactions_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('btc_temp_transactions_update', args=(self.pk,))


class btc_transaction(models.Model):
    # Fields
    transaction_id = models.CharField(max_length=400, null=True)
    amount = models.FloatField(null=True)
    tx_fee = models.FloatField(null=True)
    confirmations = models.SmallIntegerField(null=True)
    comment = models.FloatField(null=True)
    broadcast_id = models.CharField(max_length=16, default=None, null=True)
    deleted = models.SmallIntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('app_name_coin_name_transaction_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('app_name_coin_name_transaction_update', args=(self.pk,))

class ltc_transaction(models.Model):
    # Author : Astuto Lee (2017-12-28)
    # table for lite coin
    transaction_id = models.CharField(max_length=400)
    amount = models.FloatField()
    tx_fee = models.FloatField()
    confirmations = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('app_name_coin_name_transaction_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('app_name_coin_name_transaction_update', args=(self.pk,))


class btc_transaction_detail(models.Model):
    # Fields
    btc_transaction = models.ForeignKey(btc_transaction, related_name='btc_transaction_id_fk', null=True)
    category = models.SmallIntegerField(null=True)
    user_id = models.SmallIntegerField(null=True)
    address = models.CharField(max_length=400, null=True)
    amount = models.FloatField(null=True)
    tx_fee = models.FloatField(null=True)
    vout = models.IntegerField(null=True)
    is_balance_updated = models.SmallIntegerField(null=True)
    transaction_id = models.CharField(max_length=400, null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('app_name_coin_name_transaction_detail_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('app_name_coin_name_transaction_detail_update', args=(self.pk,))

class ltc_transaction_detail(models.Model):
    # Author : Astuto Lee (2017-12-28)
    # table for lite coin
    # Fields
    ltc_Transaction = models.ForeignKey(ltc_transaction, related_name='ltc_transaction_id_fk')
    category = models.SmallIntegerField()
    user_id = models.SmallIntegerField()
    address = models.CharField(max_length=400)
    amount = models.FloatField()
    tx_fee = models.FloatField()
    vout = models.IntegerField()
    is_balance_updated = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('app_name_coin_name_transaction_detail_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('app_name_coin_name_transaction_detail_update', args=(self.pk,))


# Author :Rasmus Lerdorf(2017_12_20)
# create code for user profile update
# start
class buy(models.Model):
    # Fields
    user = models.ForeignKey(User)
    units = models.DecimalField(decimal_places=8, max_digits=30)
    remain_buy_units = models.DecimalField(decimal_places=8, max_digits=30)
    bid = models.DecimalField(decimal_places=8, max_digits=30)
    fees = models.DecimalField(decimal_places=8, max_digits=30)
    total = models.DecimalField(decimal_places=8, max_digits=30)
    status = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk


class sell(models.Model):
    # Fields
    user = models.ForeignKey(User)
    units = models.DecimalField(decimal_places=8, max_digits=30)
    remain_buy_units = models.DecimalField(decimal_places=8, max_digits=30)
    ask = models.DecimalField(decimal_places=8, max_digits=30)
    fees = models.DecimalField(decimal_places=8, max_digits=30)
    total = models.DecimalField(decimal_places=8, max_digits=30)
    status = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk


class btc_widthdraw_error(models.Model):
    # Fields
    broadcast_id = models.CharField(max_length=16, null=True)
    error = models.CharField(max_length=40, null=True)
    custom_detail = models.CharField(max_length=100, null=True)
    transaction_hash = models.CharField(max_length=100, null=True)
    user = models.ForeignKey(User, null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk
# end
