from django import forms


class widthdraw_form(forms.Form):
    txfee = forms.DecimalField(required=True, decimal_places=8)
    amount = forms.DecimalField(required=True, decimal_places=8)
    to_address = forms.CharField(max_length=36, required=True)
    password = forms.CharField(required=True)
    currency = forms.CharField(required=True)

    def clean(self):
        cleaned = super(widthdraw_form, self).clean()
        txfee = cleaned.get('txfee')
        amount = cleaned.get('amount')
        to_address = cleaned.get('to_address')
        password = cleaned.get('password')
        currency = cleaned.get('currency')

        if txfee == "":
            raise forms.ValidationError("Provide coin tax fee is mandatory.")
        elif amount == "":
            raise forms.ValidationError("Provide coin total amount is mandatory.")
        elif to_address == "":
            raise forms.ValidationError("Provide coin address is mandatory.")
        elif currency == "":
            raise forms.ValidationError("Provide currency is mandatory.")
        elif password == "":
            raise forms.ValidationError("Provide password is mandatory.")