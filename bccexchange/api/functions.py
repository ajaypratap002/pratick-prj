from bccexchange.myconstants import MESSAGES # import for use constants

def custom_jwt_response(token, user=None, request=None):

    import jwt
    from .models import User
    jwt = jwt.decode(token, verify=False)
    e=User.objects.filter(email=user).values().first()
    if e['account_verified']==False:
        return {
        'message': MESSAGES.ACCOUNT_NOT_VERIFIED
        }
    elif e['status']==1:
        return {
        'token': token,
        'user': {'id':e['id'],'username':e['username'],'email':e['email'],'firstname':e['firstname'],'lastname':e['lastname'],"mobile":e['mobile'],"country":e['country'],"zipcode":e['zipcode'],"account_verified":e['account_verified']}
        }
    else:
        return {
        'message': MESSAGES.ACCOUNT_NOT_ACTIVE
        }