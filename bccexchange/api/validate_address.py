from hashlib import sha256

digits58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'


def decode_base58(bc, length):
    n = 0
    for char in bc:
        n = n * 58 + digits58.index(char)
    return n.to_bytes(length, 'big')


class add_valid():

    def check_btc(btc_address):
        try:
            bcbytes = decode_base58(btc_address, 25)
            if btc_address[0:1] == "1":
                return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]
            else:
                return False
        except Exception:
            return False

    def check_bcc(bcc_address):
        try:
            bcbytes = decode_base58(bcc_address, 25)
            if bcc_address[0:1] == "8":
                return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]
            else:
                return False
        except Exception:
            return False

    def check_ltc(ltc_address):
        try:
            bcbytes = decode_base58(ltc_address, 25)
            if ltc_address[0:1] == "L":
                return bcbytes[-4:] == sha256(sha256(bcbytes[:-4]).digest()).digest()[:4]
            else:
                return False
        except Exception:
            return False
